package ru.nile.einksearch;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	private Button btnSearch, btnBuy;
	private TextView text, spisok;		
	private AutoCompleteTextView textView;
	
	private DB db;	
	
	private Cursor cursor;
	private Cursor cdisp;
	private Cursor cUrl;
	
	private ListView lvData;	
	private String[] helpers;
	private String brandModel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		text = (TextView) findViewById(R.id.text2);
		spisok = (TextView) findViewById(R.id.textView2);
		btnSearch = (Button) findViewById(R.id.btnSearch);
		btnBuy = (Button) findViewById(R.id.btnBuy);
		lvData = (ListView) findViewById(R.id.listView1);
		lvData.setOnItemClickListener(listener);
		
        //��������� ����������� � ��
        db = new DB(this);
        db.open();
        
        //�������� ������ �� ������� � ������ ������
        cursor = db.getBrand();        
        
        helpers = new String[cursor.getCount()];
       
		//�������� �������� � ��������, �������� ������ ������, ���� � ���, ������ ������ ������ 0 �����
		if(cursor.moveToFirst()) {
			//���������� ������ �������� �� ����� �� �������
			
			int idIndex = cursor.getColumnIndex("_id");
			int brandIndex = cursor.getColumnIndex("brand");
			int modelIndex = cursor.getColumnIndex("model");
			int i=0;
			
			do {
				//�������� �������� �� ������� ��������� ������� �������� � ������� � ���
				Log.d("Mylog", "brand = " + cursor.getString(brandIndex) +
						", model = " + cursor.getString(modelIndex));
				helpers[i]=(cursor.getString(brandIndex) + ", " + cursor.getString(modelIndex));
				i++;
				
			} while (cursor.moveToNext());				
		}
		
		//������� ������ ��������� � ��������� aoutocomplete	
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, helpers);		
        
        textView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
        textView.setAdapter(adapter);     
        
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.btnSearch:
			brandModel = textView.getText().toString();
			if(!brandModel.equals("")) {
				String[] arr = brandModel.split(", ");			
				//��������� �������� �����
				if(arr.length > 1) {
					//������ ������ �� ������ ��������
					cdisp = db.getDisp(arr[1].toString());		
						
					//���� ����, ���-�� �������, �� ���������� ������ ������, ������������ �
					if(cdisp.moveToFirst()){
						//������ ������� ��������� ��� ������� ������� "������ ����������� �������"
						spisok.setVisibility(0);
						btnBuy.setEnabled(true);
						spisok.setText("������ ����������� �������\n\t��� " + brandModel);
					
						//��������� ������ � ���������� ���������
						String[] from = {"disp"};
						int[] to = {R.id.text1};
		
						SimpleCursorAdapter sAdapter = new SimpleCursorAdapter(this, R.layout.item, cdisp, from, to);
						lvData.setAdapter(sAdapter);
					}
					else {
						warnT();
					}
				}
				else {
					warnT();
				}
			}
			else {
				warnT();
			}
		break;
		case R.id.btnBuy:
			//���������� ������ �� �����
			Intent Email = new Intent(Intent.ACTION_SEND);
		    Email.setType("text/email");
		    Email.putExtra(Intent.EXTRA_EMAIL, new String[] { "part@market-play.ru" });
		    Email.putExtra(Intent.EXTRA_SUBJECT, "����� ������ ��� " + brandModel);
		    Email.putExtra(Intent.EXTRA_TEXT, "������������, ���� �������� ����� ��� " + brandModel + "\n\n������ ���������� ����� Android ����������.");
		    startActivity(Intent.createChooser(Email, "��������� e-mail"));
		    startActivity(Email);
			break;
		}
	}
	
	public OnItemClickListener listener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
			// ��������� ������ � ������������ � ��������� ��������
			cdisp.moveToPosition(position);
			
			//������ ������ ��� ����������� URL �������� � ����������� � �������
			cUrl = db.getUrl(cdisp.getString(1).toString());
			
			//���� ���-�� �������, ��������� ������
			if(cUrl != null) {
				if(cUrl.moveToFirst()) {
					
					onDialog(cUrl.getString(1).toString());
				}
				else {
					//�� ������� ����� ��������� ���������� � �������
					warnT();
				}				

			}
			
		}
	};
	
	public void onDialog(String url) {
		final String goUrl = url.trim();
		//������ ����� ����� ������ ��������� builder � �������� this
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		//��������� �������������� � �������
		builder.setMessage("������� �� ���� ���������� ���������?");
		//��������� ��������� � �������
		builder.setTitle("������� �� ����");
		//��������� ������ � �������
		//builder.setIcon(R.drawable.ic_launcher);
		
		//������ ������ YES ������� � ���������� � �������
		builder.setPositiveButton("��", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//Toast.makeText(getApplicationContext(), url, Toast.LENGTH_LONG).show();
				Intent browseIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(goUrl));
				startActivity(browseIntent);										
			}
		});
		
		//������ ����� No � ���������� ��� ��
		builder.setNegativeButton("���", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();					
			}
		});
		//��������� ������ ������ � ������� ������� Back �� ���������
		builder.setCancelable(false);
		builder.show();
	}	

    
    private void warnT() {
    	//��������� ������ ������ ��������
    	btnBuy.setEnabled(false);
    	//��������� �� ��������� ����� ������
    	Toast errorInput;
    	errorInput =  Toast.makeText(this, "������ �� �������, ���������� ��������� ����� � ���������� ���������� ������ �� ������.", Toast.LENGTH_LONG);
    	errorInput.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
    	errorInput.show();
    }

}
