package ru.nile.einksearch;

import org.xmlpull.v1.XmlPullParser;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.widget.Toast;
import android.database.sqlite.SQLiteOpenHelper;


public class DB extends Activity {
	
	private static final String DB_NAME = "eink";
	private static final int DB_VERSION = 29;
	private static final String DB_TABLE = "eink";
	
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_BRAND = "brand";
	public static final String COLUMN_MODEL = "model";
	public static final String COLUMN_DISP = "disp";
	public static final String COLUMN_URL = "url";
	
	private static final String DB_CREATE = "create table " + DB_TABLE + "(" + COLUMN_ID + " integer primary key autoincrement, "
		+ COLUMN_BRAND + " text, " + COLUMN_MODEL + " text, " + COLUMN_DISP + " text, " + COLUMN_URL + " text);";
	
	private final Context mCtx;
	
	private DBHelper mDBHelper;
	private static SQLiteDatabase mDB;
	
	//� ����������� ������� ��������
	public DB(Context ctx) {
		mCtx=ctx;				
	}
	//������� �����������
	public void open() {		
		mDBHelper = new DBHelper(mCtx, DB_NAME, null, DB_VERSION);
		mDB = mDBHelper.getWritableDatabase();		
	}
	//������� ����������� ���� ������ ����������
	public void close() {
		if(mDBHelper != null) {
			mDBHelper.close();
		}
	}
	//�������� ��� ������ �� �������
	public Cursor getAllData() {
		return mDB.query(DB_TABLE, null, null, null, null, null, null);
	}
	
	public Cursor getBrand() {
		boolean distinct = true;
		String[] columns = new String[] {"brand", "model"};		
		String orderBy = "model ASC";		
		return mDB.query(distinct, DB_TABLE, columns, null, null, null, null, orderBy, null, null);
	}
	
	public Cursor getDisp(String disp) {		
		String selection = "model = ?";
		String[] selectionArg = {disp};
		//Toast.makeText(mCtx, selectionArg[0].toString(), Toast.LENGTH_LONG).show();
		return mDB.query(DB_TABLE, new String[] {"_id", "disp"}, selection, selectionArg, null, null, null);
	}
	
	public Cursor getUrl(String url) {		
		String selection = "disp = ?";
		String[] selectionArg = {url};
		//Toast.makeText(mCtx, selectionArg[0].toString(), Toast.LENGTH_LONG).show();
		return mDB.query(DB_TABLE, new String[] {"_id", "url"}, selection, selectionArg, null, null, null);
	}
	
	//�������� ������ � �������
	public void addRec(String brand, String model, String disp, String url) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_BRAND, brand);
		cv.put(COLUMN_MODEL, model);
		cv.put(COLUMN_DISP, disp);
		cv.put(COLUMN_URL, url);
		
		mDB.insert(DB_TABLE, null, cv);
	}
	//������� ������ �� �������
	public static void delRec(long id) {
		//mDB.execSQL("DROP TABLE IF EXIST " + DB_TABLE);
		//mDB.execSQL(DB_CREATE);
		mDB.delete(DB_TABLE, COLUMN_ID + " = " + id, null);
	}
	
	
	//����� ��� �������� � ���������� ��
	private class DBHelper extends SQLiteOpenHelper {
		
		Context xmlContext;

		public DBHelper(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
			
			xmlContext = context;
		}
		
		@Override
		public void onCreate(SQLiteDatabase db) {			
			//������ � ��������� ��			
			db.execSQL(DB_CREATE);
			//������ ������ �� xml ����� � sqlite			
			xmlParse(db, xmlContext);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			
			if(oldVersion == 28 & newVersion == 29) {
				db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
				db.execSQL(DB_CREATE);
				//������ ������ �� xml ����� � sqlite
				xmlParse(db, xmlContext);
			}
			
		}
		
	}
	
	private void xmlParse(SQLiteDatabase db, Context xcontext) {				
		ContentValues cv = new ContentValues();
		//begin parse
		try {
			XmlPullParser parser = xcontext.getResources().getXml(R.xml.eink);	
						
			String data = "";	
			
			while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
				
				if(parser.getEventType() == XmlPullParser.TEXT) {
					
					data += parser.getText() + ", ";					
				}
				else if(parser.getEventType() == XmlPullParser.END_TAG && parser.getName().equals("row")) {
					
					String[] arr=data.split(", ");					
				    //addRec(arr[1].toString(), arr[2].toString(), arr[3].toString(), arr[4].toString());					
					cv.clear();
					cv.put("_id", arr[0].toString());
					cv.put("brand", arr[1].toString());
					cv.put("model", arr[2].toString());
					cv.put("disp", arr[3].toString());
					cv.put("url", arr[4].toString());
				    db.insert(DB_TABLE, null, cv);	    
				    cv.clear();
				    
					data="";
				}				
				parser.next();				
			}			
		}
		catch (Throwable t) {
			Toast.makeText(this, "Error loading XML document: " + t.toString(), Toast.LENGTH_SHORT).show();			
		}
		//end parse
	}	

}
